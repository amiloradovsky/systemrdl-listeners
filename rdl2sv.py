#!/usr/bin/env python3

import argparse, sys, math

from systemrdl import RDLCompiler, RDLCompileError, RDLListener, RDLWalker
from jinja2 import Environment

parser = argparse.ArgumentParser \
    (description = 'Generate SystemVerilog packages from SystemRDL files.')

parser.add_argument ('input_files', metavar = 'RDL_FILE',
                     type = str, nargs = '+',
                     help = 'SystemRDL input files')
parser.add_argument ('-p', '--package', metavar = 'PACKAGE_NAME',
                     type = str, nargs = 1,
                     help = 'output package name')
parser.add_argument ('-w', '--address-width', metavar = 'WIDTH',
                     type = int, nargs = 1,
                     dest = 'address_width', default = [32],
                     help = "width of the registers' addresses")
parser.add_argument ('-i', '--indent-step', metavar = 'NCHARS',
                     type = int, nargs = 1,
                     dest = 'indent_step', default = [2],
                     help = "indentation spacing (default is two spaces)")
parser.add_argument ('-a', '--attribute-size', metavar = 'NCHARS',
                     type = int, nargs = 1,
                     dest = 'attribute_size', default = [1],
                     help = "width of the attribute for each bit (default is one bit)")

args = parser.parse_args ()

indent_step   = args.indent_step[0]
address_width = args.address_width[0]
attr_size     = args.attribute_size[0]
input_files   = args.input_files
package       = args.package[0] if args.package else \
    '_'.join (input_files[-1].split ('.')[:-1] + ['pkg'])
# If none has been given explicitly,
# generate the package name from the last file name.

rdlc = RDLCompiler ()

try:
    for input_file in input_files:
        rdlc.compile_file (input_file)

    root = rdlc.elaborate ()
except RDLCompileError:
    sys.exit (1)

class Address_Parameter_Listener (RDLListener):
    def __init__ (self):
        self.indent = indent_step - 1
    def print (self, *args, **kwargs):
        indent_arg = [' ' * self.indent]
        print (*indent_arg, *args, **kwargs)
    def print_register (self, name, address):
        width = address_width
        assert (math.log2 (max (1, address)) < width)
        self.print ("localparam", "%-*s" % (32, name), "=",
                    "%d'h%0*x;" % (width, int (width / 4), address))

    def enter_Addrmap (self, node):
        mapid = node.get_path (hier_separator = '_').upper ()
        self.print_register (mapid + "_TOTAL_OCTETS", node.total_size)
    def exit_Addrmap (self, node):
        pass

    def enter_Reg (self, node):
        regid = node.get_path (hier_separator = '_',
                               empty_array_suffix = "_{dim:d}").upper ()
        self.print_register (regid + "_SADR", node.raw_absolute_address)
        self.print_register (regid + "_EADR", node.raw_absolute_address + node.total_size)
    def exit_Reg (self, node):
        pass

class Address_Validity_Listener (RDLListener):
    def __init__ (self):
        self.indent = indent_step - 1
    def print (self, *args, **kwargs):
        indent_arg = [' ' * self.indent]
        print (*indent_arg, *args, **kwargs)

    def enter_Addrmap (self, node):
        self.wvalid_regs = []
        self.rvalid_regs = []
    def exit_Addrmap (self, node):
        width = address_width
        # NOTE: perhaps a template engine would be a better solution
        wvalid_str = "||\n\
            ".join (["offset == %-*s" % (32, s)
                               for s in self.wvalid_regs])
        wvalid = "\n\
  function logic valid_waddr (input logic[%d:%d] offset);\n\
    return (%s);\n\
  endfunction: valid_waddr" % (width - 1, 0, wvalid_str)
        rvalid_str = "||\n\
            ".join (["offset == %-*s" % (32, s)
                               for s in self.rvalid_regs])
        rvalid = "\n\
  function logic valid_raddr (input logic[%d:%d] offset);\n\
    return (%s);\n\
  endfunction: valid_raddr" % (width - 1, 0, rvalid_str)
        self.print ("//", "write & read access test functions,")
        self.print ("//", "also generated from the SystemRDL file")
        print (wvalid, rvalid, sep = "\n")

    def enter_Reg (self, node):
        self.reg_is_sw_writable = True  # being optimistic...
        self.reg_is_sw_readable = True  # about the access type
    def exit_Reg (self, node):
        regid = node.get_path (hier_separator = '_',
                               empty_array_suffix = "_{dim:d}").upper ()
        reg_name = regid + "_SADR"
        if self.reg_is_sw_writable: self.wvalid_regs.append (reg_name)
        if self.reg_is_sw_readable: self.rvalid_regs.append (reg_name)

    def enter_Field (self, node):
        if not node.is_sw_writable:
            self.reg_is_sw_writable = False
        if not node.is_sw_readable:
            self.reg_is_sw_readable = False
    def exit_Field (self, node):
        pass

reg_template = """\
{% for reg in regs | reverse %}\
{% if reg.fields and not reg.size %}\
  typedef struct packed {{ "{" }}\
{% if reg.long_name and (reg.long_name != reg.name) %}\
 // {{ reg.long_name }}\
{% endif %}
{% if reg.desc %}\
{% for line in reg.desc %}\
    // {{ line }}
{% endfor %}\
{% endif %}\
{% for field in reg.fields | reverse %}
{% if field.encode %}\
    enum logic {{ field.range }} {
{% for k, v in field.encode %}\
      {{ k }} = {{ v.value }}{{ "" if loop.last else "," }}
{% endfor %}\
    } {{ field.name }};\
{% else %}\
    logic {{ field.range }} {{ field.name }};\
{% endif %}\
{% if field.long_name and (field.long_name != field.name) %}\
 // {{ field.long_name }}\
{% endif %}
{% if field.desc %}\
{% for line in field.desc %}\
    // {{ line }}
{% endfor %}\
{% endif %}\
{% endfor %}
  }\
{% if reg.dimensions %}\
{% for dimension in reg.dimensions %} {{ dimension }}{% endfor %}\
{% endif %}\
 {{ reg.type_name }}_t;
{{ "" if loop.last else "\n" }}\
{% endif %}\
{% endfor %}
"""
reg_renderer = Environment ().from_string (reg_template)

svt_template = """\
{% macro print_reg (reg) %}\
{# two cases: arrays and structures #}\
{% if reg.size and not reg.fields %}\
    logic [7:0] {{ reg.size }} {{ reg.name }};
{% endif %}\
{% if reg.fields and not reg.size %}\
    struct packed { // {{ reg.address_range }}\
{% if reg.long_name and (reg.long_name != reg.name) %}\
 {{ reg.long_name }}\
{% endif %}
{% if reg.desc %}\
{% for line in reg.desc %}\
      // {{ line }}
{% endfor %}\
{% endif %}\
{% for field in reg.fields | reverse %}
{% if field.encode %}\
      enum logic {{ field.range }} {
{% for k, v in field.encode %}\
        {{ k }} = {{ v.value }}{{ "" if loop.last else "," }}
{% endfor %}\
      } {{ field.name }};\
{% else %}\
      logic {{ field.range }} {{ field.name }};\
{% endif %}\
{% if field.long_name and (field.long_name != field.name) %}\
 // {{ field.long_name }}\
{% endif %}
{% if field.desc %}\
{% for line in field.desc %}\
      // {{ line }}
{% endfor %}\
{% endif %}\
{% endfor %}
    }\
{% if reg.dimensions %}\
{% for dimension in reg.dimensions %} {{ dimension }}{% endfor %}\
{% endif %}\
 {{ reg.name }}; // {{ reg.type_name }}
{% endif %}\
{% endmacro %}\
  typedef struct packed\
{% if long_name and (long_name != name) %}\
 // {{ long_name }}\
{% endif %}
  {
{% if desc %}\
{% for line in desc %}\
    // {{ line }}
{% endfor %}
{% endif %}\
{% for reg in regs | reverse %}\
{% if reg.members is not defined %}\
{{ print_reg (reg) }}\
{% else %}\
    struct packed { // {{ reg.address_range }}

{% for r in reg.members | reverse %}\
{{ print_reg (r) | indent (2, True) }}
{% endfor %}\
    } {{ reg.name }};
{% endif %}\
{{ "" if loop.last else "\n" }}\
{% endfor %}
  } {{ name }};
"""
svt_renderer = Environment ().from_string (svt_template)

class SV_Header_Listener (RDLListener):
    def bit_range (self, x, y):
        return "[" + str (x) + ":" + str (y) + "]"

    def enter_Addrmap (self, node):
        self.offset = 0
        self.regs = []
        self.in_regfile = False
    def exit_Addrmap (self, node):
        print (reg_renderer.render (regs = self.regs), end = "")
        print (svt_renderer.render (name = node.get_path_segment (),
                                    long_name = node.get_property ("name"),
                                    desc = node.get_property ("desc").splitlines () \
                                    if node.get_property ("desc") else None,
                                    regs = self.regs))

    def enter_Regfile (self, node):
        self.in_regfile = True
        offset_diff = node.raw_absolute_address - self.offset
        if offset_diff != 0:
            self.regs.append ({'name': "__unknown_region_at_%02x" % self.offset,
                               'size': "['h%x:1]" % offset_diff})
            self.offset += offset_diff
        self.regfile = []
    def exit_Regfile (self, node):
        self.in_regfile = False
        self.regs.append ({'name': node.get_path_segment \
                           (empty_array_suffix = "[{dim:d}]"),
                           'address_range': "('h%x, 'h%x]" % (node.raw_absolute_address + node.total_size,
                                                              node.raw_absolute_address),
                           'members': self.regfile})

    def enter_Reg (self, node):
        offset_diff = node.raw_absolute_address - self.offset
        if offset_diff != 0:
            register = {'name': "__unknown_region_at_%02x" % self.offset,
                        'size': "['h%x:1]" % offset_diff}
            if self.in_regfile:
                self.regfile.append (register)
            else:
                self.regs.append (register)
            self.offset += offset_diff

        self.bit_offset = 0
        self.reg_fields = []
    def exit_Reg (self, node):
        width = node.size * 8  # from octets to bits
        if self.bit_offset < width:
            field = {'range': self.bit_range (width - 1, self.bit_offset),
                     'name': "__padding__"}
            self.reg_fields.append (field)

        register = {'name': node.get_path_segment (empty_array_suffix = ""),
                    'long_name': node.get_property ("name"),
                    'desc': node.get_property ("desc").splitlines () \
                    if node.get_property ("desc") else None,
                    'type_name': node.type_name,
                    'dimensions': list (map (lambda n: self.bit_range (n - 1, 0),
                                             node.array_dimensions) if node.is_array else []),
                    'address_range': "('h%x, 'h%x]" % (node.raw_absolute_address + node.total_size,
                                                       node.raw_absolute_address),
                    'fields': self.reg_fields}
        if self.in_regfile:
            self.regfile.append (register)
        else:
            self.regs.append (register)

        self.offset += node.total_size

    def enter_Field (self, node):
        current = min (node.msb, node.lsb)
        bit_offset_diff = current - self.bit_offset
        if bit_offset_diff != 0:
            field = {'range': self.bit_range (current - 1, self.bit_offset),
                     'name': "__padding_at_" + str (self.bit_offset)}
            self.reg_fields.append (field)
        self.bit_offset += bit_offset_diff + node.width

        encode = node.get_property ("encode")
        field = {'range': self.bit_range (node.msb, node.lsb),
                 'name': node.get_path_segment (),
                 'long_name': node.get_property ("name"),
                 'desc': node.get_property ("desc").splitlines () \
                 if node.get_property ("desc") else None,
                 'encode': list (encode.__members__.items ()) \
                 if encode else None}
        self.reg_fields.append (field)

class Attribute_Listener (RDLListener):
    def __init__ (self):
        self.indent = indent_step - 1
        self.hold_print = False
        self.held_string = ""
    def bit_range (self, x, y):
        return "[" + str (x) + ":" + str (y) + "]"

    def print (self, *args, appended = False, **kwargs):
        indent_arg = [' ' * self.indent] if self.indent != 0 and not appended else []
        if self.hold_print:
            self.held_string += ' '.join (indent_arg + list (args)) \
                + (kwargs['end'] if 'end' in kwargs else '\n')
        else:
            print (*indent_arg, *args, **kwargs)
    def name_comment (self, node, appended = False):
        short_name = node.get_path_segment ()
        long_name = node.get_property ("name")
        mark = [] if appended else ["//"]
        return (mark + [long_name + " attributes"]) if long_name != short_name else []
    def print_desc (self, node):
        if node.get_property ("desc"):
            for line in node.get_property ("desc").splitlines ():
                self.print ("//", line)
        if not self.hold_print: self.print (appended = True)

    def enter_Addrmap (self, node):
        self.print ("typedef", "struct", "packed",
                    *self.name_comment (node))
        self.print ("{")
        self.indent += indent_step
        self.offset = 0
        self.regs = []
    def exit_Addrmap (self, node):
        for reg in self.regs: print (*reg, sep = '')
        self.indent -= indent_step
        self.print ("}", node.get_path_segment () + "_attrs" + ";")

    def enter_Reg (self, node):
        self.reg = []
        self.hold_print = True
        self.held_string = ""

        offset_diff = node.raw_absolute_address - self.offset
        if offset_diff != 0:
            self.print ("logic", self.bit_range (8 * attr_size - 1, 0),
                        "['h%x:1]" % offset_diff,
                        "__unknown_region_at_%02x;" % self.offset)
            self.offset += offset_diff
            self.regs.insert (0, [self.held_string])
            self.held_string = ""

        self.print ("struct", "packed", "{", "//",
                    "('h%x, 'h%x]" % (node.raw_absolute_address + node.total_size,
                                      node.raw_absolute_address),
                    *self.name_comment (node, appended = True))
        self.indent += indent_step
        self.bit_offset = 0
        self.reg_fields = []

        self.reg.append (self.held_string)
        self.hold_print = False
    def exit_Reg (self, node):
        width = node.size * 8  # from octets to bits
        if self.bit_offset < width:
            diff = width - self.bit_offset
            self.hold_print = True
            self.held_string = ""
            self.print ("logic",
                        self.bit_range (width - 1, self.bit_offset) + \
                        self.bit_range (attr_size - 1, 0),
                        "__padding__;")
            self.reg_fields.insert (0, self.held_string)
            self.hold_print = False

        self.reg += self.reg_fields
        self.hold_print = True
        self.held_string = ""

        self.indent -= indent_step
        indices = map (lambda n: self.bit_range (n - 1, 0),
                       node.array_dimensions) if node.is_array else []
        self.print ("}", *indices,
                    node.get_path_segment (empty_array_suffix = "") + ";",
                    "//", node.type_name)
        self.offset += node.total_size

        self.reg.append (self.held_string)
        self.hold_print = False
        self.regs.insert (0, self.reg)

    def enter_Field (self, node):
        current = min (node.msb, node.lsb)
        bit_offset_diff = current - self.bit_offset
        if bit_offset_diff != 0:
            self.hold_print = True
            self.held_string = ""
            self.print ("logic",
                        self.bit_range (current - 1, self.bit_offset) + \
                        self.bit_range (attr_size - 1, 0),
                        "__padding_at_" + str (self.bit_offset) + ";")
            self.reg_fields.insert (0, self.held_string)
            self.hold_print = False
        self.bit_offset += bit_offset_diff + node.width

        self.hold_print = True
        self.held_string = ""
        self.print ("logic",
                    self.bit_range (node.msb, node.lsb) + \
                    self.bit_range (attr_size - 1, 0),
                    end = " ")
        self.print (node.get_path_segment () + ";",
                    *self.name_comment (node), appended = True)
    def exit_Field (self, node):
        self.reg_fields.insert (0, self.held_string)
        self.hold_print = False

template = """\
{% macro print_fields (register, comma) %}\
{% for field in register.fields %}\
      {{ field.name }}: {{ field.value }}\
{{ "" if loop.last and not comma else "," }}
{% endfor %}\
{% endmacro %}\
  localparam {{ type }} {{ name }} = '{
{% for register in registers %}\
    {{ register.name }}: '{
{% if not register.indices %}\
{{ print_fields (register, True) }}\
{% else %}\
{% for index in register.indices %}\
      {{ index }}: '{
{{ print_fields (register, False) | indent (2, True) }}\
      },
{% endfor %}\
{% endif %}\
      default: '0
    },
{% endfor %}\
    default: '0
  };
"""
renderer = Environment ().from_string (template)

class Constant_Listener (RDLListener):
    def enter_Addrmap (self, node):
        self.type = node.get_path_segment () + "_attrs"
        self.name = node.get_path_segment () + "_defs"
        self.registers = []
    def exit_Addrmap (self, node):
        print (renderer.render (type = self.type, name = self.name,
                                registers = self.registers))

    def enter_Reg (self, node):
        self.fields = []
    def exit_Reg (self, node):
        name = node.get_path_segment (array_suffix = "",
                                      empty_array_suffix = "")
        indices = list (range (node.array_dimensions[0])) if node.is_array else None
        # NOTE: multi-dimensional arrays are not supported
        self.registers.append ({'name': name,
                                'fields': self.fields,
                                'indices': indices})

    def enter_Field (self, node):
        property = node.get_property ("attr_p")
        attr_value = property.value if property is not None else 0
        value = "{%d{%d'%s}}" % (node.width, attr_size,
                                 "b{:0{}b}".format (attr_value,
                                                    attr_size))
        self.fields.append ({'name': node.get_path_segment (),
                             'value': value})

print ("// -*- mode: Verilog; tab-width: 4 -*-")
print ()
print ("package %s;" % package)
print ()
RDLWalker ().walk (root, Address_Parameter_Listener ())
print ()
RDLWalker ().walk (root, Address_Validity_Listener ())
print ()
RDLWalker ().walk (root, SV_Header_Listener ())
print ()
RDLWalker ().walk (root, Attribute_Listener ())
print ()
RDLWalker ().walk (root, Constant_Listener ())
print ()
print ("endpackage: %s" % package)

# TODO: regfiles and arrays
