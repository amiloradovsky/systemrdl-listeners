BEGIN{ printf ("package %s_pkg;\n\n", pkgname) }
{ printf ("  localparam int unsigned %s = %s;\n", $1, $NF) }
END{ printf ("\nendpackage: %s_pkg\n", pkgname) }
