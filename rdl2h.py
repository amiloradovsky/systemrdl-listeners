#!/usr/bin/env python3

import argparse, sys, math

from systemrdl import RDLCompiler, RDLCompileError, RDLListener, RDLWalker
from jinja2 import Environment

parser = argparse.ArgumentParser \
    (description = 'Generate C header from SystemRDL files.')

parser.add_argument ('input_files', metavar = 'RDL_FILE',
                     type = str, nargs = '+',
                     help = 'SystemRDL input files')
parser.add_argument ('-p', '--package', metavar = 'PACKAGE_NAME',
                     type = str, nargs = 1,
                     help = 'output package name')

args = parser.parse_args ()

input_files   = args.input_files
package       = args.package[0] if args.package else \
    '_'.join (input_files[-1].split ('.')[:-1] + ['h']).upper ()

rdlc = RDLCompiler ()

try:
    for input_file in input_files:
        rdlc.compile_file (input_file)

    root = rdlc.elaborate ()
except RDLCompileError:
    sys.exit (1)

def clog2 (x): return math.ceil (math.log2 (x))

addr_template = """\
{% for r in registers %}\
#define {{ r.name }} (volatile uint{{ r.size }}_t *) ({{ base }} + {{ r.address }})
{% if r.long_name is defined %}\
// {{ r.long_name }} ^
{% endif %}\
{% if r.desc is defined %}\
{% for line in r.desc %}\
{{ "/* " if loop.first else "   " }}{{ line }}{{ " */" if loop.last else "" }}
{% endfor %}\
{% endif %}\
{% endfor %}
"""
addr_renderer = Environment ().from_string (addr_template)

class Address_Parameter_Listener (RDLListener):
    def enter_Addrmap (self, node):
        self.mapid = node.get_path (hier_separator = '_').upper ()
        print ("#define", self.mapid + "_TOTAL_OCTETS",
               "(0x%x)" % node.total_size)
        print ()
        self.registers = []
        self.length = 0
    def exit_Addrmap (self, node):
        for r in self.registers:
            r.update ({'name': "%-*s" % (self.length, r['name'])})
        print (addr_renderer.render (base = self.mapid + "_BASE",
                                     registers = self.registers))

    def enter_Reg (self, node):
        regid = node.get_path (hier_separator = '_',
                               array_suffix = "",
                               empty_array_suffix = "")
        name = regid.upper () + "_PTR"
        self.length = max (self.length, len (name))
        register = {'name': name, 'size': node.size * 8,
                    'address': "0x%02x" % node.raw_absolute_address}

        long_name = node.get_property ("name")
        if long_name != node.get_path_segment (array_suffix = "",
                                               empty_array_suffix = ""):
            register['long_name'] = long_name

        desc = node.get_property ('desc')
        if desc is not None:
            register['desc'] = desc.splitlines ()

        self.registers.append (register)

class Fields_Parameters_Listener (RDLListener):
    def exit_Reg (self, node):
        print ()

    def enter_Field (self, node):
        field = node.get_path (hier_separator = '_',
                               array_suffix = "",
                               empty_array_suffix = "").upper ()
        offset = node.low
        size = 1 + node.high - node.low
        print ("#define", "%-*s" % (48, field + "_OFFSET"),
               "(%d)" % offset)
        print ("#define", "%-*s" % (48, field + "_SIZE"),
               "(%d)" % size)
        print ("#define", "%-*s" % (48, field + "_MASK"),
               "(0x%x)" % (~(~0 << size) << offset))

        long_name = node.get_property ("name")
        if long_name != node.get_path_segment (array_suffix = "",
                                               empty_array_suffix = ""):
            print ("//", long_name)

        desc = node.get_property ('desc')
        if desc is not None:
            for line in desc.splitlines (): print ("//", line)

enum_template = """\
enum {{ name }}_e {
{% for k, v in items %}\
  {{ k }} = {{ v.value }},
{% endfor %}\
};

"""
enum_renderer = Environment ().from_string (enum_template)

regs_template = """\
{% macro print_reg (reg) %}\
{% if reg.name is not defined and reg.position is defined %}\
  uint8_t __reserved_at_{{ reg.position }} [{{ reg.size }}];
{% else %}\
  volatile uint{{ reg.width }}_t {{ reg.name }};
{% endif %}\
{% endmacro %}\
#pragma pack (push, 1)

{% for name, regfile in regfiles %}\
typedef struct {{ name }}_s {
{% for reg in regfile %}\
{{ print_reg (reg) }}\
{% endfor %}\
} {{ name }}_t;
{{ "" if loop.last else "\n" }}\
{% endfor %}\

typedef struct {{ name }}_s {
{% for register in registers %}\
{% if register.name is not defined and register.position is defined %}\
  uint8_t __reserved_at_{{ register.position }} [{{ register.size }}];
{% elif register.name is defined and register.members is not defined %}\
  volatile uint{{ register.width }}_t {{ register.name }};
{% else %}\
  struct {
{% for reg in register.members %}\
{{ print_reg (reg) | indent (2, True) }}\
{% endfor %}\
  } {{ register.name }};
{% endif %}\
{% endfor %}\
} {{ name }}_t;

#pragma pack (pop)
"""
regs_renderer = Environment ().from_string (regs_template)

class Registers_Structure_Listener (RDLListener):
    def enter_Addrmap (self, node):
        self.offset = 0
        self.registers = []
        self.regfiles = {}
        self.in_regfile = False
    def exit_Addrmap (self, node):
        print (regs_renderer.render (name = node.get_path_segment (),
                                     registers = self.registers,
                                     regfiles = self.regfiles.items ()))

    def enter_Regfile (self, node):
        self.in_regfile = True
        self.rf_offset = self.offset  # store
        offset_diff = node.raw_absolute_address - self.offset
        if offset_diff != 0:
            self.registers.append ({'position': "%02x" % self.offset,
                                    'size': "0x%x" % offset_diff})
            self.offset += offset_diff
        self.regfile = []
    def exit_Regfile (self, node):
        self.in_regfile = False
        self.offset = self.rf_offset  # restore
        self.regfiles[node.type_name] = self.regfile
        self.registers.append ({'name': node.get_path_segment \
                                (empty_array_suffix = "[{dim:d}]"),
                                'members': self.regfile})
        self.offset += node.total_size

    def enter_Reg (self, node):
        offset_diff = node.raw_absolute_address - self.offset
        if offset_diff != 0:
            register = {'position': "%02x" % self.offset,
                        'size': "0x%x" % offset_diff}
            if self.in_regfile:
                self.regfile.append (register)
            else:
                self.registers.append (register)
            self.offset += offset_diff
    def exit_Reg (self, node):
        register = {'name': node.get_path_segment \
                    (empty_array_suffix = "[{dim:d}]"),
                    'width': node.size * 8}
        if self.in_regfile:
            self.regfile.append (register)
        else:
            self.registers.append (register)
        self.offset += node.total_size

    def enter_Field (self, node):
        encode = node.get_property ("encode")
        if encode is not None:
            # mangle the names a bit for nicer spacing in the output
            length = max (map (len, encode.__members__.keys ()))
            items = encode.__members__.items ()
            print (enum_renderer.render \
                   (name = node.get_path (hier_separator = '_'),
                    items = [("%-*s" % (length, k), v) for k, v in items]))

print ("/* -*- mode: C; tab-width: 4 -*- */\n")
print ("#include <sys/types.h>\n")
print ("#ifndef", package)
print ("#define", package)
print ()
RDLWalker ().walk (root, Address_Parameter_Listener ())
print ()
RDLWalker ().walk (root, Fields_Parameters_Listener ())
print ()
RDLWalker ().walk (root, Registers_Structure_Listener ())
print ()
print ("#endif")
