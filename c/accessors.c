#include "accessors.h"
#include "io.h"

#define reg_field_accessors(width, suffix)                      \
  u ## width                                                    \
  reg ## width ## _field_get (u ## width offset,                \
                              u ## width size,                  \
                              volatile u ## width *address)     \
  {                                                             \
    u ## width x = read ## suffix (address);                    \
    return (x >> offset) & MASK0 (size);                        \
  }                                                             \
                                                                \
  void                                                          \
  reg ## width ## _field_set (u ## width offset,                \
                              u ## width size,                  \
                              volatile u ## width *address,     \
                              u ## width value)                 \
  {                                                             \
    u ## width mask = MASK (offset, size);                      \
    value <<= offset;                                           \
    u ## width x = read ## suffix (address);                    \
    u ## width y = (x & ~mask) | (value & mask);                \
    write ## suffix (y, address);                               \
  }                                                             \

reg_field_accessors ( 8, b)
reg_field_accessors (16, w)
reg_field_accessors (32, l)
reg_field_accessors (64, q)

u64
reg_field_get (unsigned reg_char_size,
               u64 offset, u64 field_bit_size, volatile u64 *address)
{
  switch (reg_char_size*CHAR_BIT)
    {
    case  8:
      return reg8_field_get  (offset, field_bit_size, (volatile u8  *) address);
    case 16:
      return reg16_field_get (offset, field_bit_size, (volatile u16 *) address);
    case 32:
      return reg32_field_get (offset, field_bit_size, (volatile u32 *) address);
    case 64:
      return reg64_field_get (offset, field_bit_size, (volatile u64 *) address);
    }
#ifdef _EBREAK
  _EBREAK ();
#else
  while (0 == 0);
#endif
}

void
reg_field_set (unsigned reg_char_size,
               u64 offset, u64 field_bit_size, volatile u64 *address, u64 value)
{
  switch (reg_char_size*CHAR_BIT)
    {
    case  8:
      return reg8_field_set  (offset, field_bit_size, (volatile u8  *) address, value);
    case 16:
      return reg16_field_set (offset, field_bit_size, (volatile u16 *) address, value);
    case 32:
      return reg32_field_set (offset, field_bit_size, (volatile u32 *) address, value);
    case 64:
      return reg64_field_set (offset, field_bit_size, (volatile u64 *) address, value);
    }
#ifdef _EBREAK
  _EBREAK ();
#else
  while (0 == 0);
#endif
}
