#ifndef ACCESSORS_H
#define ACCESSORS_H

#include <stdint.h>
#include <limits.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

// the size is assumed to be positive (unsigned)
#define MASK0(size) ((~0ULL) >> (64 - size))
#define MASK(offset, size) ((MASK0 (size)) << offset)

#define reg_field_accessors_proto(width)                        \
  u ## width                                                    \
  reg ## width ## _field_get (u ## width offset,                \
                              u ## width size,                  \
                              volatile u ## width *address);    \
  void                                                          \
  reg ## width ## _field_set (u ## width offset,                \
                              u ## width size,                  \
                              volatile u ## width *address,     \
                              u ## width value);                \

reg_field_accessors_proto ( 8)
reg_field_accessors_proto (16)
reg_field_accessors_proto (32)
reg_field_accessors_proto (64)

u64
reg_field_get (unsigned width,
               u64 offset, u64 size, volatile u64 *address);
void
reg_field_set (unsigned width,
               u64 offset, u64 size, volatile u64 *address, u64 value);

#endif /* ACCESSORS_H */
