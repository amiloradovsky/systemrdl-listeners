
ifndef SV_DIR
SV_DIR = .
endif

ifndef C_DIR
C_DIR = .
endif

TARGETS = \
	$(SV_DIR)/$(MAP_NAME)_pkg.sv $(C_DIR)/$(MAP_NAME).h $(C_DIR)/$(MAP_NAME).c \
	$(SV_DIR)/$(RDL_NAME)_pkg.sv $(C_DIR)/$(RDL_NAME).h

all: $(TARGETS)

$(SV_DIR)/$(MAP_NAME)_pkg.sv: $(MAP_NAME).map
	sed -f $(RDL_TOOLS)/strip.sed $^ | awk -v pkgname=$(basename $<) -f $(RDL_TOOLS)/map2sv.awk > $@

$(C_DIR)/$(MAP_NAME).h: $(MAP_NAME).map
	sed -f $(RDL_TOOLS)/strip.sed $^ | awk -f $(RDL_TOOLS)/map2h.awk > $@

$(C_DIR)/$(MAP_NAME).c: $(MAP_NAME).map
	sed -f $(RDL_TOOLS)/strip.sed $^ | awk -f $(RDL_TOOLS)/map2c.awk > $@

$(MAP_NAME).sed: $(MAP_NAME).map
	sed -f $(RDL_TOOLS)/strip.sed $^ | awk -f $(RDL_TOOLS)/map2sed.awk | tac > $@

$(RDL_NAME).rdl: $(MAP_NAME).sed $(RDL_NAME).rdl.template
	sed -f $(MAP_NAME).sed < $(RDL_NAME).rdl.template > $(RDL_NAME).rdl

$(SV_DIR)/$(RDL_NAME)_pkg.sv: $(RDL_NAME).rdl
	$(RDL_TOOLS)//rdl2sv.py $(SV_OPTS) $^ > $@

$(C_DIR)/$(RDL_NAME).h: $(RDL_NAME).rdl
	$(RDL_TOOLS)//rdl2h.py $(C_OPTS) $^ > $@

clean:
	rm -f $(TARGETS) $(MAP_NAME).sed $(RDL_NAME).rdl
