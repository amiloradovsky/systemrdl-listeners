
// Getting the engine state

field dma_complete
{
  name = "Completion flag";
  desc = "High means the operation has been completed, engine is idle";
  hw = w;
  sw = r;
};
field dma_failure
{
  name = "Failure flag";
  desc = "High means the operation has failed, interrupt has been sent, the engine may be still running though...";
  hw = w;
  sw = r;
};
field dma_testing
{
  name = "Test field";
  desc = "For testing only";
  hw = na;
  sw = rw;
};
reg status_reg
{
  name = "Status register";
  desc = "Indicates the state of the DMA engine";
  dma_testing  testing [7:6];
  dma_failure  read_failure [3:3];
  dma_complete read_complete[2:2];
  dma_failure  write_failure [1:1];
  dma_complete write_complete[0:0];
  // 0 -- in progress, 1 -- success, 2 and 3 -- failure
};

field dma_complete_clear
{
  name = "Completion clear flag";
  desc = "Write 1 to clear the completion status";
  hw = r;
  sw = rw;
};
field dma_failure_clear
{
  name = "Failure clear flag";
  desc = "Write 1 to clear the failure status";
  hw = r;
  sw = rw;
};
reg status_clear_reg
{
  name = "Status clear register";
  desc = "Clears the state of the DMA engine";
  dma_failure_clear  read_failure [3:3];
  dma_complete_clear read_complete[2:2];
  dma_failure_clear  write_failure [1:1];
  dma_complete_clear write_complete[0:0];
};

// Setting the command id/tag

field command_tag
{
  desc = "Command which the data belong to";
  fieldwidth = CMD_TAG_WIDTH;
  hw = r;
  sw = w;
};
reg tag_reg
{
  command_tag tag;
};

// Setting the data offset

field offset_addr
{
  desc = "Offset to refer to the command's data";
  fieldwidth = HOST_OFFSET_WIDTH;
  hw = r;
  sw = w;
};
reg offset_reg
{
  offset_addr addr;
};

// Setting the range

field dma_address
{
  desc   = "Intermemory address in the words of DATA_WIDTH, only accessible by the DMA block";
  fieldwidth = DEV_OFFSET_WIDTH;
  hw     = r;
  sw     = w;
};
reg start_reg
{
  dma_address addr;
};
reg finish_reg
{
  dma_address addr;
};

// Issuing the command

enum dma_command_e
  {
    none  = 32'd0  { desc = "No command present"; };
    write = 32'd1  { desc = "Start writing the (random) data"; };
    read  = 32'd2  { desc = "Start reading & checking the data"; };
  };
field dma_command
{
  desc   = "Command to be executed by the DMA block (write or read & check)";
  encode = dma_command_e;
  fieldwidth = 32;
  hw     = r;
  sw     = w;
};
reg command_reg
{
  dma_command code[31:0];
};

// Register block to operate the DMA engine

addrmap dma_regs
{
  name = "Controller Registers (DMA)";
  desc = "This address map only contains some registers for setting up a trivial DMA operations.";

  default accesswidth = 32;
  addressing = regalign;
  alignment = 4;
  littleendian;
  lsb0;

  status_reg  status             @0x08;
  status_clear_reg status_clear  @0x0c;
  tag_reg     id                 @0x10;
  offset_reg  offset;
  start_reg   start;
  finish_reg  finish;
  command_reg command;

  reg { field { hw = w; sw = r; } f; } arr_test[TEST_X][TEST_Y];
  reg { field { hw = r; sw = w; } g; } after_arr_test;
};
